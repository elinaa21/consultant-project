import './Login.css';


function Login() {
    return (
        <section className="text-center text-lg-start login-container align-middle">
            <div className="card mb-3 login-box ">
                <div className="row g-0 d-flex align-items-center">
                    <div className="col-lg-4 d-none d-lg-flex">
                        <img
                            src="/images/logo.jpeg"
                            alt="icon"
                            className="w-100 rounded-t-5 rounded-tr-lg-0 rounded-bl-lg-5"
                        />
                    </div>
                <div className="col-lg-8">
                    <div className="card-body py-5 px-md-5">
                    <form>
                        <div className="form-outline mb-4">
                            <input type="email" id="form2Example1" className="form-control" />
                            <label className="form-label" htmlFor="form2Example1">Login</label>
                        </div>

                        <div className="form-outline mb-4">
                            <input type="password" id="form2Example2" className="form-control" />
                            <label className="form-label" htmlFor="form2Example2">Password</label>
                        </div>

                        <button type="button" className="btn btn-primary btn-block mb-4">Sign in</button>
                    </form>

                    </div>
                </div>
                </div>
            </div>
        </section>
  );
}

export default Login;